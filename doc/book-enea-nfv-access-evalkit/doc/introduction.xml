<?xml version="1.0" encoding="ISO-8859-1"?>
<chapter id="intro_evalkit221">
  <title>Introduction</title>

  <para>The NFV Access EvalKit offered by Enea contains software that can be
  used to deploy an SD-WAN setup. The branch-to-branch setup uses two
  opensource based VNFs: flexiWAN and pfSense. These VNFs are provided as
  examples and the configuration used only works on the documented
  setup.</para>

  <para>Enea NFV Access for the universal Customer Premise Equipment (uCPE) is
  a virtualization and management platform, which allows end-users to
  introduce, instantiate, and run third-party VNFs onto their systems.</para>

  <para>The solution is comprised of two major components working in close
  cooperation:</para>

  <itemizedlist>
    <listitem>
      <para>The Enea NFV Access Run-Time Platform, which acts as the host for
      Virtualized Network Functions (VNFs) and provides management over
      NETCONF.</para>
    </listitem>

    <listitem>
      <para>The Enea uCPE Manager, a solution that runs on an external server,
      providing VNF Management functionality and managing large numbers of
      uCPEs.</para>
    </listitem>
  </itemizedlist>

  <para>The current Enea NFV Access solution provides a working and deployable
  configuration as an example for branch-to-branch connection setup using the
  flexiWAN and pfSense VNFs service chained together on a uCPE device.</para>

  <para>This document will present all information required to replicate the
  use case described in the user's environment. The first part of this manual
  uses the GUI mode of the uCPE Manager to detail the steps in order to
  reproduce the use cases, while the chapters thereafter use the automation
  framework.</para>

  <note>
    <para>All VNF configurations should be seen as example configurations
    working in Enea internal lab and the user must update these files with the
    configuration data needed according to his network setup. Particularities
    are described in Appendix A.</para>

    <para>This document assumes the user is familiar with Enea NFV Access and
    has read the Enea NFV Access Getting Started manual before continuing with
    the following.</para>
  </note>

  <section id="def_acro_evalkit221">
    <title>Definitions and Acronyms</title>

    <section>
      <title>uCPE Manager</title>

      <para>The Enea uCPE Manager is an EMS/NMS platform providing VNF
      Management capabilities for NFV Access devices. The uCPE Manager can be
      deployed on a Linux (CentOS) based physical or virtual server.</para>
    </section>

    <section>
      <title>Automation Framework</title>

      <para>The Automation Framework consists of a set of tooling and a
      collection of Python based scripts that can be used to automate the
      process of onboarding a VNF with all of the required configuration for
      day zero deployment at scale.</para>

      <para>This tooling calls the auto generated REST API that's exposed on
      the uCPE Manager as a north bound interface.</para>
    </section>
  </section>
</chapter>