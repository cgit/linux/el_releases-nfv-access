<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<chapter id="intro_ucpe">
  <title>Introduction</title>

  <para><remark>INFO <emphasis role="bold">eltf_params_updated.xml</emphasis>
  contains many parameters in the book, also in the open source books, and
  MUST be created FROM <emphasis
  role="bold">eltf_params_template.xml</emphasis>. The parameters are
  automatically used in the books, via xi:include statements, similar to how
  parameters from pardoc-distro.xml are included in the book. Read the file
  <emphasis role="bold">eltf_params_updated_template_how_to_use.txt</emphasis>
  for important details about formats and how to do! The idea is that ELTF
  will auto-create/update it.</remark></para>

  <para>Enea NFV Access for universal Customer Premise Equipment (uCPE) is a
  virtualization and management platform, which allows end-users to introduce,
  instantiate, and run third-party VNFs onto their systems. It is comprised of
  two major components working in close cooperation:</para>

  <itemizedlist>
    <listitem>
      <para>The Enea NFV Access Run-Time Platform, which acts as the host for
      Virtualized Network Functions (VNFs) and provides management over
      NETCONF.</para>
    </listitem>

    <listitem>
      <para>The Enea uCPE Manager, a solution that runs on an external server,
      providing VNF Management functionality and managing large numbers of
      uCPEs.</para>
    </listitem>
  </itemizedlist>

  <section id="nfv_access">
    <title>Enea NFV Access Run Time Platform</title>

    <para>Enea NFV Access Run Time Platform is a lightweight,
    multi-architecture virtualization platform, supporting Virtual Machines
    (KVM / QEMU) and container(s) (Docker). Designed for a low footprint and
    fast boot by only providing essential functionality.</para>

    <para>The NFV Access virtualized data plane has DPDK and accelerated OVS
    as its primary components. The data plane is highly optimized for
    scenarios where high throughput and low latency are needed.</para>

    <para>VNF Runtime Management, orchestration integration, and FCAPS are
    provided through EdgeLink NETCONF.</para>

    <figure>
      <title>VNF Space</title>

      <mediaobject>
        <imageobject>
          <imagedata align="center" fileref="images/vnf_space.png" scale="80" />
        </imageobject>
      </mediaobject>
    </figure>
  </section>

  <section id="ucpe_manager">
    <title>Enea uCPE Manager</title>

    <para>Enea uCPE Manager is a control center application providing a full
    FCAPS and VNF management experience through a GUI and REST API. It can be
    deployed on a host or a virtual machine. CentOS and Windows are supported
    host platforms by default. The uCPE Manager uses a southbound EdgeLink
    NETCONF protocol to connect and manage uCPE devices.</para>

    <para>Enea uCPE Manager provides the following key features:</para>

    <itemizedlist>
      <listitem>
        <para>VNF On-boarding</para>
      </listitem>

      <listitem>
        <para>VNF Management</para>
      </listitem>

      <listitem>
        <para>FCAPS</para>
      </listitem>

      <listitem>
        <para>Zero Touch Provisioning</para>
      </listitem>

      <listitem>
        <para>Alarms / Events management and monitoring</para>
      </listitem>
    </itemizedlist>
  </section>
</chapter>