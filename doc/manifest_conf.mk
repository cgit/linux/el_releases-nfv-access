# To be included in Makefile
# NOTE: MANIFESTHASH needs to be changed to final release tag in format refs/tags/ELnnn before a release
#       The values are shown in the release info
#       The manifest is used to fetch information into the release info from the distro files
MANIFESTHASH      ?= refs/tags/Enea_NFV_Access_2.2.2
	#change the above value later to refs/tags/ELnnn (?)
MANIFESTURL       := ssh://gerrit.enea.se:29418/linux/manifests/el_manifests-nfv-access
PROFILE_NAME      := Enea NFV Access
