#!/bin/bash

# This script locates build artifacts in a provided list of directories and copies
# them into a release structure as defined in a release content definition file.

usage() {
    cat <<EOF >&2

Usage:
   $(basename $0) [OPTIONS] -o <output directory> <build directory>...

OPTIONS:
   -i           Ignore missing files
   -p           Print the release structure without creating it
   -r <file>    Set release definition file (default: RELEASE_CONTENT)

EOF
    exit
}

RC_def="./RELEASE_CONTENT"

while getopts "io:pr:" name; do
    case $name in
        i) ignore_missing=1 ;;
        o) RC_dir=$OPTARG ;;
        r) RC_def=$OPTARG ;;
        p) print=1 ;;
        ?) usage ;;
    esac
done

if [ ! "$RC_dir" ] && [ ! "$print" ]; then
    echo "ERROR: Output directory not set"
    usage
fi

if [ -d "$RC_dir" ]; then
    echo "ERROR: Output directory $RC_dir already exists"
    exit 1
fi

if [ ! -f "$RC_def" ]; then
    echo "ERROR: release definition file '$RC_def' does not exist"
    exit 1
fi

echo "Release definition: $RC_def"
echo -e "Output directory: $RC_dir\n"

shift $(( OPTIND - 1 ))
builddirs=("$@")

while read line; do
    if [ -z "$line" ] || [[ $line = \#* ]]; then continue; fi

    IFS=: read -r srcfile dstpath _ <<< $line

    if [ ! -z "$print" ]; then
        echo $RC_dir/$dstpath
        continue
    fi

    srcpath=$(find -L ${builddirs[@]} -wholename "*$srcfile" -print -quit)

    if [ "$srcpath" ]; then
        dstdir=$RC_dir/$(dirname "$dstpath")
        dstfile=$(basename "$dstpath")

        mkdir -p $dstdir
        cp -Lpv $srcpath $dstdir/$dstfile
        pushd $dstdir
        md5sum $dstfile > $dstfile.md5
        popd
    else
        if [ "$ignore_missing" ]; then
            echo "WARNING: File '$srcfile' not found"
        else
            echo "ERROR: File '$srcfile' not found"
            exit 1
        fi
    fi

done < $RC_def

echo -e "\nDone, RC directory: $RC_dir"
